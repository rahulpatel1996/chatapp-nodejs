const express = require('express');
const bodyParser = require('body-parser');
const app = express();
const http = require('http').Server(app);
const io = require('socket.io')(http);
const path = require("path");
const routes = require('./js/registration');
const ChatList = require('./models/chatSchema');
const cors = require('cors');
const url = require('url');

const port = process.env.port || 5000;

const users = {};
app.use(cors());
app.set('view engine', 'ejs');

app.use(express.static(path.join(__dirname, "views")));
app.use(express.static(path.join(__dirname, "/public")));
app.use(express.static(path.join(__dirname, "/js")));

app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json())
app.use('/', routes);

app.get('/', (req, res) => {
    return res.redirect('index.html');
});

io.on('connection', socket => {
    socket.on('new-user-joined', name => {
        console.log(`new user = ${name} joined the chat`);
        users[socket.id] = name;

        const saveCollection = new ChatList({
            username: users[socket.id]
        });
        saveCollection.save();
        socket.broadcast.emit('user-joined', name);
    });

    socket.on('send', message => {
        socket.broadcast.emit('receive', { message: message, name: users[socket.id] })
    });

    socket.on('disconnect', message => {
        socket.broadcast.emit('left', users[socket.id]);
        delete users[socket.id];
    });
});

http.listen(port, () => {
    console.log(`app listening at http://localhost:${port}`)
});

