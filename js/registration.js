const express = require('express');
const chatController = require('../controller/chatController');
const { check, validationResult } = require("express-validator");
const router = express.Router();
const async = require('async');
const crypto = require('crypto');
const UserList = require("../models/schema.js");
var nodemailer = require('nodemailer');
const md5 = require('md5');
const jwt = require('jsonwebtoken');


router.get('/registration', chatController.get_registration);
router.get('/login', chatController.get_login);

router.get('/user-list', (req, res) => {

    jwt.verify(req.query.token, 'EmailToken', function (err, decoded) {
        console.log(decoded);
        if (err) {
            res.send("Invalid Token");
        }
        else {
            UserList.find({ email: { $ne: decoded.email } }, function (err, users) {
                var userMap = {};
                res.render("user-list", { userMap: users });
            });
        }
    });
});

router.post('/registration', [
    check("username", "Please Enter a Valid Username at least 2 character")

        .not()
        .isEmpty(),
    check("email", "Please enter a valid email").isEmail(),
    check("password", "Please enter a valid password at least 6 character").isLength({
        min: 6
    })
], chatController.post_registration);


router.post("/login", [
    check("email", "Please enter a valid email").isEmail(),
    check("password", "Please enter a valid password").isLength({
        min: 6

    })

], chatController.post_login);

router.get('/forgot-password', function (req, res) {

    res.render('forgot-password', { user: req.user });
});


router.post('/forgot-password', function (req, res, next) {
    async.waterfall([
        function (done) {
            crypto.randomBytes(20, function (err, buf) {
                const token = buf.toString('hex');
                done(err, token);
            });
        },
        function (token, done) {
            UserList.findOne({ email: req.body.email }, function (err, user, req) {
                if (!user) {
                    console.log('No account with that email address exists.');
                    return res.redirect("/login");
                }
                user.resetPasswordToken = token;
                user.resetPasswordExpires = Date.now() + 3600000; //1 hour

                user.save(function (err) {
                    done(err, token, user);
                });

            });
        },
        function (token, user, done) {
            const smtpTransporter = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'nitesh.synsoft@gmail.com',
                    pass: 'synsoft@1998'
                }
            });

            const mailOptions = {
                from: 'nitesh.synsoft@gmail.com', //sender
                to: req.body.email,
                subject: 'Password Reset Notification',
                html: '<h3>You are receiving  the reset of the password on your account. ' + '\n' + ' Please click on the following link, or paste into your browser to complete the password reset process. </h3>' + ' \n ' + 'http://' + req.headers.host + '/reset-password/' + token + '\n\n' +
                    '<h4> If you did not request this, please ignore this email and your password will remain unchanged </h4>'
            };


            smtpTransporter.sendMail(mailOptions, function (err, req) {
                console.log('mail sent');
                console.log('success', 'An e-mail has been sent to ' + user.email + 'with further instructions to reset password.');
                done(err, 'done');
            });

        }
    ], function (err) {
        res.redirect('/login');
    });
});

router.get('/reset-password/:token', function (req, res) {
    UserList.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } }, function (err, user) {
        if (!user) {
            // req.flash('error', 'Password reset token is invalid or has expired.');
            console.log('Password reset token is invalid or has expired.');

            return res.redirect('/forgot-password');
        }
        res.render('reset-password', {
            user: req.user
        });
    });
});

router.post('/reset-password/:token', function (req, res) {
    async.waterfall([
        function (done) {
            UserList.findOne({ resetPasswordToken: req.params.token, resetPasswordExpires: { $gt: Date.now() } },
                function (err, user) {
                    if (!user) {
                        // req.flash('error', 'Password reset token is invalid or has expired.');
                        console.log("ERROR");
                        return res.redirect('/login');
                    }
                    console.log("yah pe aaya");

                    user.password = md5(req.body.password);
                    console.log("password pe aaya hai");
                    user.resetPasswordToken = undefined;
                    user.resetPasswordExpires = undefined;

                    user.save(function (err) {
                        // req.logIn(user, function (err) {
                        done(err, user);
                        // });
                    });

                    // user.updateOne(function (err) {
                    //     done(err, user);
                    // });
                });
        },
        function (user, done) {
            var smtpTransport = nodemailer.createTransport({
                service: 'Gmail',
                auth: {
                    user: 'nitesh.synsoft@gmail.com',
                    pass: 'synsoft@1998'
                }
            });
            var mailOptions = {
                to: user.email,
                from: 'nitesh.synsoft@gmail.com',
                subject: 'Your password has been changed',
                text: 'Hello,\n\n' +
                    'This is a confirmation that the password for your account ' + user.email + ' has just been changed.\n'
            };
            smtpTransport.sendMail(mailOptions, function (err) {
                // req.flash('success', 'Success! Your password has been changed.');
                console.log('Success! Your password has been changed.');

                done(err);
            });
        }
    ], function (err) {
        res.redirect('/');
    });
});

module.exports = router;
console.log("module exports");