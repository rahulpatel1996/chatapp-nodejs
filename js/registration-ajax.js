$(document).ready(function () {
    $("#registrationForm").on("submit", function (e) {
        e.preventDefault();
        console.log("click button");
        var form = $(this);

        $.ajax({

            type: form.attr('method'),
            url: "/registration",
            data: form.serialize()
        }).done(function (data) {
            console.log(data.result);
            alert("Registration Successfull");
            window.location.href = 'login.html';

        }).fail(function (data) {

            console.log(data);
            var data = jQuery.parseJSON(data.responseText);

            $(".err").html(data.msg);
            console.log(data.msg);

            for (var i = 0; i < data.errors.length; i++) {

                $('#' + data.errors[i].param).next().html('<span style="color:red">' + data.errors[i].msg + '</span>');

            };
        });
    });
});


// for (var i = 0; i < data.length; i++) {
//     $('#' + data[i].key).next().html('<span>' + data[i].msg + '</span>');
// }