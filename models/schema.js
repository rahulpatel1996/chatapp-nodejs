//connection ko require kar na hai
const mongoose = require('mongoose');
const mongooseDB = require('./db');
// const bcrypt = require('bcrypt');


const schema = new mongoose.Schema({
    username: {
        type: String,
        required: true,
        minlength: [2, "Name minimum have 2 words"],
    },

    email: {
        type: String,
        trim: true,
        unique: true,
        required: "Email is required",
    },

    password: {
        type: String,
        require: true,
        minlength: [6, "Password minimum length is 6"],
    },

    resetPasswordToken: String,
    resetPasswordExpires: Date

});

// schema.pre('save', function (next) {
//     var user = this;
//     var SALT_FACTOR = 5;

//     if (!user.isModified('password')) return next();

//     bcrypt.genSalt(SALT_FACTOR, function (err, salt) {
//         if (err) return next(err);

//         bcrypt.hash(user.password, salt, null, function (err, hash) {
//             if (err) return next(err);
//             user.password = hash;
//             next();
//         });
//     });
// });

// schema.methods.comparePassword = function (candidatePassword, cb) {
//     bcrypt.compare(candidatePassword, this.password, function (err, isMatch) {
//         if (err) return cb(err);
//         cb(null, isMatch);
//     });
// };

const UserList = new mongoose.model("UserList", schema);
module.exports = UserList;
console.log("schema called");