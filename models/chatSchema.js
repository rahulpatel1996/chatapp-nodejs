const mongoose = require('mongoose');
const mongooseDB = require('./db');


const chatSchema = new mongoose.Schema({
    username: String,
    userMessage: {
        type: String
    }
});

const ChatList = new mongoose.model("ChatList", chatSchema);
module.exports = ChatList;
console.log("ChatList called");
