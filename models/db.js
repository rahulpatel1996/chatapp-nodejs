const express = require('express');
const mongoose = require('mongoose');

const mongooseDB = mongoose.connect('mongodb://localhost:27017/Nitesh', {
    useNewUrlParser: true, useUnifiedTopology: true, useCreateIndex: true, useFindAndModify: false
}).then(() => console.log("mongoose connected"))
    .catch((error) => console.log("error is", error));

module.exports = mongooseDB;
