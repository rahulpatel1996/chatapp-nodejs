const express = require('express');
const UserList = require("../models/schema.js");
const bodyParser = require('body-parser');
const { check, validationResult } = require("express-validator");
const app = express();
const path = require("path");
const md5 = require('md5');
const crypto = require("crypto");
const router = require('../js/registration.js');
const { response } = require('express');
const jwt = require('jsonwebtoken');

app.set('view engine', 'ejs');

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(path.join(__dirname, "views")));
app.use(express.static(path.join(__dirname, "/public")));

const get_registration = (req, res) => {
    return res.redirect('registration.html');
};

const post_registration = async (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    const {
        username,
        email,
        password
    } = req.body;

    try {
        let user = await UserList.findOne({
            email
        });

        if (user) {
            return res.status(400).json({
                msg: "Email is already exist"
            });
        }

        user = new UserList({
            username,
            email,
            password
        });

        user.password = await md5(password);

        console.log(user.password);

        await user.save();
        return res.status(200).json({
            result: user
        });
    }

    catch (err) {
        console.log(err.message);
        res.status(500).send("Error in Saving");
    }
};

const get_login = (req, res) => {
    return res.redirect('login.html');
}

const post_login = async (req, res) => {

    const errors = validationResult(req);
    if (!errors.isEmpty()) {
        return res.status(400).json({
            errors: errors.array()
        });
    }

    UserList.findOne(
        {
            $and: [
                { email: req.body.email },
                { password: crypto.createHash('md5').update(req.body.password).digest('hex') }
            ]
        },

        (err, result) => {

            if (err) {
                res.status(500).send(err);
                return;
            }

            if (!result) {
                return res.status(400).json({
                    message: "Invalid Email or password"
                });

            } else {
                UserList.find({ email: { $ne: req.body.email } }, function (err, users) {

                    // res.status(200).json({
                    //     getUser: users
                    // });
                    var token = jwt.sign({ email: req.body.email }, 'EmailToken');
                    console.log(token);
                    var userMap = {};
                    res.send({ userMap: users, token });
                });
            };
        }
    );
}

module.exports = {
    get_registration,
    post_registration,
    get_login,
    post_login
}
console.log("chatController called");
